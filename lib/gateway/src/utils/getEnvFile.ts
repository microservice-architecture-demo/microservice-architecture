export function getEnvFile(): string {
  if (process.env.NODE_ENV === 'production') {
    return '.env.prod';
  }

  if (process.env.NODE_ENV === 'test') {
    return '.env.test';
  }

  if (!process.env.NODE_ENV) {
    throw new Error('Please set NODE_ENV');
  }

  return '.env.dev';
}
