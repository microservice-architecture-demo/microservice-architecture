import {NestFactory} from '@nestjs/core';
import {DocumentBuilder, SwaggerModule} from '@nestjs/swagger';
import {ConfigService} from '@nestjs/config';
import {Router} from 'express';

import {AppModule} from './app.module';
import {isDevEnv} from './utils/isDevEnv';
import {version} from '../package.json';

const swaggerAppPath = 'swagger-ui.html';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const configService: ConfigService = app.get(ConfigService);

  const port = configService.get<string>('PORT');
  const appName = configService.get<string>('APP_NAME');
  const appPrefix = `/api/${appName}`;

  /**
   * Swagger configuration
   */
  const options = new DocumentBuilder()
  .addServer(isDevEnv() ? '/' : `/${appName}`)
  .setTitle('Api Documentation')
  .setVersion(version)
  .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup(swaggerAppPath, app, document);

  /**
   * Set global app prefix
   */
  app.setGlobalPrefix(appPrefix);

  /**
   * Proxy urls for swagger-gateway
   */
  const router = Router();
  router.get('/v2/api-docs', (req, res) => {
    res.status(200).json(document);
  });
  router.get(`/${appName}/v2/api-docs`, (req, res) => {
    res.status(200).json(document);
  });
  app.use(router);

  await app.listen(port);
}

bootstrap();
