import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import {EurekaModule} from "nestjs-eureka";
import {ConfigModule} from "@nestjs/config";
import {getEnvFile} from "./utils/getEnvFile";

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: ['.env', getEnvFile()],
      isGlobal: true,
    }),
    EurekaModule.forRoot({
      service: {
        name: 'gateway',
        port: 8081,
      },
      eureka: {
        host: 'localhost',
        port: '8761',
        servicePath: '/eureka/apps/',
        maxRetries: 30,
      },
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
