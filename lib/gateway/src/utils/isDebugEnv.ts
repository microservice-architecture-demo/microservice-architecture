export function isDebugEnv(): boolean {
  return process.env.DEBUG === 'true';
}
